from pages.web.components.page_builder.page_builder_modules_base import *


class PageBuilderSingleCard(PageBuilderModuleBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='SingleCard']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False


class PageBuilderMasonryCardCollection(PageBuilderMasonryCardCollectionBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='MasonryCardCollection']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False


class RowModules(RowModulesBase):

    _column_body_locator = (By.CSS_SELECTOR, "div.fl-col-content.fl-node-content.ui-sortable")

    def column_body(self, index):
        _ = self.find_elements(*self._column_body_locator)
        return _[index]


class SideBarMenu(SidebarMenuBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='SidebarMenu']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False
