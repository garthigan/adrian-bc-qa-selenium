from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage

class CreateNewCustomPage(CreateNewPagePage):

    URL_TEMPLATE = "/wp-admin/post-new.php"
