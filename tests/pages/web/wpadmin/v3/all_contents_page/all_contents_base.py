from selenium.webdriver.common.by import By
from pages.web.staff_base import StaffBasePage
from selenium.common.exceptions import NoSuchElementException


class AllContentsBasePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/edit.php"

    _heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _add_new_locator = (By.CSS_SELECTOR, "a.page-title-action")
    _all_filter_locator = (By.CSS_SELECTOR, "a[href*='all_posts']")
    _mine_filter_locator = (By.CSS_SELECTOR, "a[href*='author']")
    _published_filter_locator = (By.CSS_SELECTOR, "a[href*='publish']")
    _drafts_filter_locator = (By.CSS_SELECTOR, "a[href*='draft']")
    _trash_filter_locator = (By.CSS_SELECTOR, "a[href*='status=trash']")
    _search_input_locator = (By.CSS_SELECTOR, "#post-search-input")
    _search_button_locator = (By.CSS_SELECTOR, "#search-submit")
    _search_results_locator = (By.CSS_SELECTOR, ".wrap > .subtitle")

    @property
    def loaded(self):
        return self.find_element(*self._heading_locator)

    @property
    def add_new(self):
        return self.find_element(*self._add_new_locator)

    @property
    def all_filter(self):
        return self.find_element(*self._all_filter_locator)

    @property
    def mine_filter(self):
        return self.find_element(*self._mine_filter_locator)

    @property
    def published_filter(self):
        return self.find_element(*self._published_filter_locator)

    @property
    def drafts_filter(self):
        return self.find_element(*self._drafts_filter_locator)

    @property
    def trash_filter(self):
        return self.find_element(*self._trash_filter_locator)

    @property
    def search_input(self):
        return self.find_element(*self._search_input_locator)

    @property
    def search_button(self):
        return self.find_element(*self._search_button_locator)

    @property
    def is_search_results_displayed(self):
        try:
            return self.find_element(*self._search_results_locator).is_displayed()
        except NoSuchElementException:
            return False
