from selenium.webdriver.common.by import By
from .base import BasePage
from pypom import Region


class SearchListsResultsPage(BasePage):

    URL_TEMPLATE = "/search?t=userlist&search_category=userlist&q={query}"

    _list_locator = (By.CSS_SELECTOR, "#bibList > div.cp_bib_list.clearfix > div > div")

    @property
    def lists(self):
        return [self.List(self, element) for element in self.find_elements(*self._list_locator)]

    class List(Region):

        _title_locator = (By.CSS_SELECTOR, "#bibList > div.cp_bib_list.clearfix > div > div > span > a")

        @property
        def title(self):
            return self.find_element(*self._title_locator)
