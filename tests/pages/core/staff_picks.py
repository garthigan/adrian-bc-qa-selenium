from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/explore/featured_lists/staff_picks
class StaffPicksPage(BasePage):

    URL_TEMPLATE = "/explore/featured_lists/staff_picks"

    _root_locator = (By.CSS_SELECTOR, "[data-test-id='featured_lists_page']")
    _explore_panel_locator = (By.CSS_SELECTOR, "[class*='cp_explore_panel']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._root_locator).is_present()
        except NoSuchElementException:
            return False

    @property
    def panel(self):
        return self.Panel(self)

    @property
    def panels(self):
        return [self.Panel(self, element) for element in self.find_elements(*self._explore_panel_locator)]

    class Panel(Region):
        _title_locator = (By.CLASS_NAME, "title")
        _links_locator = (By.CSS_SELECTOR, "div[class='link'] > a")

        @property
        def title(self):
            return self.find_element(*self._title_locator)

        @property
        def links(self):
            return self.find_elements(*self._links_locator)
