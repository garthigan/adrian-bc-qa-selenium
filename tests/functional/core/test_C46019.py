import pytest
import allure
import sure
import sys
sys.path.append('tests')
from datetime import date, timedelta
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.account import AccountPage
from pages.core.pickup_location import PickupLocationPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46019: Change default pickup location, place hold")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46019", "TestRail")
class TestC46019:
    def test_C46019(self):

        home_page = HomePage(self.driver, "https://epl.demo.bibliocommons.com").open()
        home_page.header.log_in("21221011111111", "1234")
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        search_term = "Doctor You"
        search_results_page = home_page.header.search_for(search_term) # , advanced_search = True)
        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].place_hold.click()
        search_results_page.search_result_items[0].pickup_location.text.should.equal("Castle Downs")
        search_results_page.header.login_state_user_logged_in.click()
        search_results_page.header.my_settings.click()
        account_page = AccountPage(self.driver)
        account_page.change_pickup_location.click()
        pickup_location_page = PickupLocationPage(self.driver)
        pickup_location_page.set_location_to("Capilano")
        pickup_location_page.save_changes.click()
        search_results_page = pickup_location_page.header.search_for(search_term)
        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].place_hold.click()
        search_results_page.search_result_items[0].pickup_location.text.should.equal("Capilano")

        # Change the default pickup location back to what it originally was:
        search_results_page = SearchResultsPage(self.driver)
        search_results_page.header.login_state_user_logged_in.click()
        search_results_page.header.my_settings.click()
        account_page = AccountPage(self.driver)
        account_page.change_pickup_location.click()
        pickup_location_page = PickupLocationPage(self.driver)
        pickup_location_page.set_location_to("Castle Downs")
        pickup_location_page.save_changes.click()
