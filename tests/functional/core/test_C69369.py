import pytest
import allure
import sure
import sys
sys.path.append('tests')
import time
import configuration.system


from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.v2.shelves import ShelvesPage


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69369: New search from the shelves availability results page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69369", "TestRail")
class TestC69369:
    def test_C69369(self):
        # Perform a Shelves Availability search, and then perform a new search from that page
        # Use the browser to navigate back, and check that the original search shows
        self.name ="21817002369272"
        self.password = "1992"
        base_url = "https://coaldale.demo.bibliocommons.com"
        home_page = HomePage(self.driver, base_url).open()
        home_page.header.log_in(self.name, self.password)
        home_page.header.login_state_user_logged_in.click()

        # Go to shelf with available items
        home_page.header.completed_shelf.click()
        shelves_page = ShelvesPage(self.driver)

        # Click Find Available Titles button
        shelves_page.find_available_button.click()
        search_results = SearchResultsPage(self.driver)

        # Assert that search results header shows correct shelf
        search_results.shelves_availability_header.text.should.equal("Titles from my Completed shelf")
        # Wait for the search result items area to populate with items
        search_results.wait.until(lambda s: len(search_results.search_result_items) > 0)

        # The titles of the search result items are stored here for later comparison
        shelves_availability_search = []
        for item in (search_results.search_result_items):
            shelves_availability_search.append(item.bib_title.text)

        # Perform new search (verify new search results are different)
        search_results.header.search_for("Book")
        search_results.wait_for_page_to_load()
        # Need to wait for the search result items to populate update to the new set of items.
        # Check that items are different by comparing titles
        search_results.wait.until(lambda s: shelves_availability_search[0] !=
                                            search_results.search_result_items[0].bib_title.text)



        new_search_results = []
        for item in (search_results.search_result_items):
            new_search_results.append(item.bib_title.text)

        (shelves_availability_search == new_search_results).should.be.false

        # Go back via browser, wait for contents to load
        self.driver.back()
        search_results.wait_for_page_to_load()
        search_results.wait.until(lambda s: len(search_results.search_result_items) == len(shelves_availability_search))
        search_results.wait.until(lambda s: shelves_availability_search[0] ==
                                            search_results.search_result_items[0].bib_title.text)

        # Verify that the original search results is showing by comparing the bib titles
        for i, item in enumerate(search_results.search_result_items):
            item.bib_title.text.should.equal(shelves_availability_search[i])

