import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.v2.search_results import SearchResultsPage
from pages.core.home import HomePage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C45995: Place a hold (Logged in, no single click holds)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/45995", "TestRail")
class TestC45995:
    def test_C45995(self):
        self.name ="21221011111111"
        self.password = "1234"

        base_url = "https://epl.demo.bibliocommons.com"
        home_page = HomePage(self.driver, base_url).open()
        home_page.header.log_in(self.name, self.password)
        home_page.header.search_for("The Humans")
        search_page = SearchResultsPage(self.driver)
        search_page.search_result_items[0].grouped_search_items[0].place_hold.click()
        search_page.search_result_items[0].wait.until(
            lambda s: search_page.search_result_items[0].is_confirm_hold_displayed)
        search_page.search_result_items[0].confirm_hold.click()
        search_page.wait.until(lambda s: search_page.search_result_items[0].is_alert_displayed)
        search_page.search_result_items[0].grouped_search_items[0].cancel_hold.click()
        search_page.search_result_items[0].confirm_cancel_hold.click()
        search_page.wait.until(lambda s: search_page.search_result_items[0].is_place_hold_displayed)
        search_page.search_result_items[0].is_alert_displayed.should.be.true
