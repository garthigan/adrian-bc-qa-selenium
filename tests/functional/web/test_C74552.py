import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_twitter_card import CreateNewTwitterCard
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.page_builder import PageBuilderPage
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage


PAGE = 'bibliocommons-settings'
USER_NAME = "CP24"
USER_ID = "1171486188978327557"


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C74552: Create new card - Twitter Card")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/74552", "TestRail")
class TestC74552:
    def test_C74552(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        self.driver.delete_all_cookies()

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating new Twitter Card
        new_twitter_card = CreateNewTwitterCard(self.driver, configuration.system.base_url_web, post_type='bw_twitter').open()
        new_twitter_card.page_heading.text.should.match("Create New Twitter Card")
        twitter_url = "https://twitter.com/" + USER_NAME + "/status/" + USER_ID       # https://twitter.com/CP24/status/1171486188978327557
        new_twitter_card.tweet_url.send_keys(twitter_url)
        new_twitter_card.grab_tweet_info.click()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_twitter_card.is_validator_message_displayed)
        new_twitter_card.is_validator_success_message_displayed.should.be.true
        new_twitter_card.validator_success_message.text.should.match("Tweet information successfully grabbed!")
        new_twitter_card.publish.click()
        new_twitter_card.is_success_message_displayed.should.be.true
        new_twitter_card.wpsidemenu.menu_all_content.click()
        new_twitter_card.wpsidemenu.submenu_all_pages.click()

        # Launching PB from existing page
        all_pages = AllPagesPage(self.driver)
        all_pages.wait.until(lambda s: all_pages.page_builder_filter.is_displayed())
        all_pages.page_builder_filter.click()
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].page_builder.click()

        # Checking card on Page Builder
        page_builder_page = PageBuilderPage(self.driver)
        if page_builder_page.builder_panel.is_panel_visible is False:
            page_builder_page.page_builder.add_content.click()
        page_builder_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(page_builder_page.builder_panel.single_card,
                                                page_builder_page.page_builder.body).perform()

        # Selecting the Twitter card that was created
        single_card = PageBuilderSingleCard(page_builder_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Twitter Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Twitter")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.choose_card_search_input.send_keys(USER_NAME)
        single_card.edit_card.select_card_by_index(0).click()
        single_card.edit_card.select_card_by_index(0).text.should.contain(USER_NAME)

        # Deleting created Twitter Card
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(USER_NAME)
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.contain(USER_NAME)
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty