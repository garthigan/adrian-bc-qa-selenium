import pytest
import allure
import sure
from datetime import datetime, timedelta
import configuration.system
import configuration.user
from mimesis import Text, Internet
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.page_builder import PageBuilderPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.wpadmin.v3.create_new_custom_page import CreateNewCustomPage
from utils.image_download_helper import *
from pages.web.components.page_builder.page_builder_modules import PageBuilderMasonryCardCollection
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from selenium.webdriver.common.keys import Keys
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from utils.selenium_helpers import click


PAGE = "bibliocommons-settings"
CUSTOM_PAGE_TITLE = ''.join(Text('en').words(quantity=3))
CUSTOM_CARD_ONE_INFO = {
    'title': ''.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=6))
}
CUSTOM_CARD_TWO_INFO = {
    'title': ''.join(Text('en').words(quantity=2)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=4))
}
IMAGE_ONE_TITLE = Text('en').word()
IMAGE_ONE_PATH = get_image_path_name(IMAGE_ONE_TITLE, ".jpg")
IMAGE_TWO_TITLE = Text('en').word()
IMAGE_TWO_PATH = get_image_path_name(IMAGE_TWO_TITLE, ".jpg")


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69757: Expired manually-placed cards")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69757", "TestRail")
class TestC69757:
    def test_C69757(self):

        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        # Logging out
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2EdPMqG", IMAGE_ONE_PATH)
        download_image("https://bit.ly/2IOmAJs", IMAGE_TWO_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new custom card
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web, post_type='bw_custom_card').open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_ONE_INFO['title'])
        new_custom_card.card_image.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_ONE_PATH)
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 120, poll_frequency=2, ignored_exceptions=[NoSuchElementException, IndexError])
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        new_custom_card.image_cropper.image_one_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible == False)
        new_custom_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_custom_card.image_cropper.is_crop_two_preview_visible.should.be.true
        new_custom_card.card_url.send_keys(CUSTOM_CARD_ONE_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_ONE_INFO['description'])
        new_custom_card.select_resource_type("Catalog Search")
        new_custom_card.scroll_to_top()
        new_custom_card.publish.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)
        self.driver.refresh()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)

        # Creating another new custom card
        new_custom_card.open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_TWO_INFO['title'])
        new_custom_card.card_image.click()
        new_custom_card.select_widget_image.upload_files_tab.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_TWO_PATH)
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        new_custom_card.image_cropper.image_one_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible == False)
        new_custom_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_custom_card.image_cropper.is_crop_two_preview_visible.should.be.true
        new_custom_card.card_url.send_keys(CUSTOM_CARD_TWO_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_TWO_INFO['description'])
        new_custom_card.select_resource_type("Catalog Search")
        new_custom_card.scroll_to_top()
        new_custom_card.publish.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)
        self.driver.refresh()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)

        delete_downloaded_image(IMAGE_ONE_PATH)
        delete_downloaded_image(IMAGE_TWO_PATH)

        # Creating a new custom page
        create_new_custom_page = CreateNewCustomPage(self.driver, configuration.system.base_url_web, post_type='page', page_type='custom').open()
        create_new_custom_page.title.send_keys(CUSTOM_PAGE_TITLE)
        create_new_custom_page.publish.click()
        create_new_custom_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Opening the custom page in the frontend
        custom_page = PageBuilderPage(self.driver, configuration.system.base_url_web + CUSTOM_PAGE_TITLE).open()
        custom_page.wpheader.page_builder.click()
        if custom_page.builder_panel.is_panel_visible == False:
            custom_page.page_builder.add_content.click()
        custom_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(custom_page.builder_panel.masonry_card_collection, custom_page.page_builder.body).perform()

        # Adding the custom card that was created, and setting 'Active until' label to it
        masonry_card_collection = PageBuilderMasonryCardCollection(custom_page)
        masonry_card_collection.content_tab.click()
        masonry_card_collection.module_heading.send_keys("Masonry Card Collection")
        masonry_card_collection.edit_card_button.click()
        masonry_card_collection.edit_card.general_tab.click()
        masonry_card_collection.edit_card.card_type.click()
        masonry_card_collection.edit_card.select_card_type("Custom Card")
        masonry_card_collection.edit_card.choose_card.click()
        masonry_card_collection.edit_card.select_card_by_title(CUSTOM_CARD_ONE_INFO['title']).click()
        masonry_card_collection.edit_card.ending.click()

        time_after_timedelta = datetime.utcnow() + timedelta(seconds=70)
        hour = time_after_timedelta.hour
        minute = time_after_timedelta.minute

        if hour >= 12:
            am_pm = "PM"
        else:
            am_pm = "AM"

        # Logic for selecting expiry time in card 1
        masonry_card_collection.edit_card.schedule_picker.today.click()
        masonry_card_collection.edit_card.schedule_picker.hour.click()
        # Passing in the 12 hour format of the hour variable
        masonry_card_collection.edit_card.schedule_picker.hour.send_keys(int(time_after_timedelta.strftime("%I")))
        masonry_card_collection.edit_card.schedule_picker.minute.click()
        masonry_card_collection.edit_card.schedule_picker.minute.send_keys(minute)
        if masonry_card_collection.edit_card.schedule_picker.am_pm.get_attribute("textContent") != am_pm:
            masonry_card_collection.edit_card.schedule_picker.am_pm.click()

        masonry_card_collection.edit_card.save.click()
        wait.until(lambda condition: masonry_card_collection.edit_card.active_label[0].is_displayed())
        masonry_card_collection.edit_card.active_label[0].get_attribute("textContent").should.contain("Active until")

        # Adding the second Custom Card that was created, and setting 'Active' label to it
        click(masonry_card_collection.add_card)
        masonry_card_collection.edit_card_button.click()
        masonry_card_collection.edit_card.general_tab.click()
        masonry_card_collection.edit_card.card_type.click()
        masonry_card_collection.edit_card.select_card_type("Custom Card")
        masonry_card_collection.edit_card.choose_card.click()
        masonry_card_collection.edit_card.select_card_by_title(CUSTOM_CARD_TWO_INFO['title']).click()
        masonry_card_collection.edit_card.save.click()
        wait.until(lambda condition: masonry_card_collection.edit_card.active_label[1].is_displayed())
        masonry_card_collection.edit_card.active_label[1].get_attribute("textContent").should.contain("Active")
        masonry_card_collection.save()
        custom_page.page_builder.done.click()
        custom_page.wait.until(lambda condition: custom_page.page_builder.publish.is_displayed())
        custom_page.page_builder.publish.click()
        wait.until(lambda condition: custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(custom_page.user_facing_modules.masonry_card_collections) == 1)

        custom_page.open()
        wait.until(lambda condition: len(custom_page.user_facing_modules.masonry_card_collections) == 1)
        wait.until(lambda condition: custom_page.user_facing_modules.masonry_card_collections[0].cards[0].card_title.get_attribute("textContent")
                                     == CUSTOM_CARD_ONE_INFO['title'])
        wait.until(lambda condition: custom_page.user_facing_modules.masonry_card_collections[0].cards[1].card_title.get_attribute("textContent")
                                     == CUSTOM_CARD_TWO_INFO['title'])

        # Waiting until the expiry time is reached, and then checking that the first card has the expired label in it
        wait.until(lambda condition: datetime.utcnow().minute == minute)
        custom_page.open()
        custom_page.user_facing_modules.masonry_card_collections[0].cards[0].card_title.get_attribute("textContent").should_not.equal(CUSTOM_CARD_ONE_INFO['title'])
        custom_page.user_facing_modules.masonry_card_collections[0].cards[0].card_title.get_attribute("textContent").should.equal(CUSTOM_CARD_TWO_INFO['title'])
        custom_page.wpheader.page_builder.click()
        custom_page.page_builder.body.click()
        wait.until(lambda condition: masonry_card_collection.edit_card.expired_label[0].is_displayed())
        wait.until(lambda condition: masonry_card_collection.edit_card.active_label[0].is_displayed())

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Logging out
        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Deleting the uploaded images from the media library
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_ONE_TITLE, Keys.RETURN)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: media_library_page.rows.should.be.empty)
        media_library_page.search_input.clear()
        media_library_page.search_input.send_keys(IMAGE_TWO_TITLE, Keys.RETURN)
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: media_library_page.rows.should.be.empty)

        # Deleting the created custom cards
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_ONE_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_CARD_ONE_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty
        all_cards_page.open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_TWO_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_CARD_TWO_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_input.send_keys(CUSTOM_PAGE_TITLE)
        all_pages_page.search_button.click()
        all_pages_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty

        self.driver.delete_all_cookies()
