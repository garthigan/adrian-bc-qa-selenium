import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from pages.core.home import HomePage
from pages.events.home_page_events import EventsHome
from pages.events.events_locations import EventLocationPage
from utils.selenium_helpers import click


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Filter locations by facilities")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56141", "TestRail")
class TestC56141:
    def test_c56141(self):
        self.base_url = "https://chipublib.demo.bibliocommons.com/events"
        HomePage(self.driver, self.base_url).open()

        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_link_displayed)

        home_page_events.hours_and_location_link.click()
        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_textbox_displayed)
        zip_code = "60605"
        home_page_events.hours_and_location_textbox.send_keys(zip_code)

        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_search_link_displayed)
        home_page_events.hours_and_location_search_link.click()

        location_page = EventLocationPage(self.driver)
        location_page.wait.until(lambda s: location_page.is_search_results_returned_count_displayed)
        store_results_count = location_page.search_results_returned_count.text
        location_page.wait.until(lambda s: location_page.library_in_list_name[0].is_displayed())
        library_name = location_page.library_in_list_name[0].text
        location_page.wait.until(lambda s: location_page.is_location_next_page_button_displayed)
        location_page.wait.until(lambda s: location_page.is_show_locations_with_filter_button_displayed)
        click(location_page.locations_with_filter_button)
        location_page.wait.until(lambda s: location_page.show_locations_list_with_checkbox[1].is_displayed())
        location_page.show_locations_list_with_checkbox[3].click()
        location_page.wait.until(lambda s: location_page.is_show_locations_list_selected_clear_filters_link_displayed)

        location_page.wait.until(lambda s: location_page.is_show_locations_with_filter_button_displayed)
        click(location_page.locations_with_filter_button)
        location_page.wait.until(lambda s: location_page.show_locations_list_with_checkbox[2].is_displayed())
        location_page.show_locations_list_with_checkbox[4].click()
        location_page.wait.until(lambda s: location_page.is_show_locations_list_selected_clear_filters_link_displayed)

        location_page.wait.until(lambda s: location_page.is_show_locations_with_filter_button_displayed)
        click(location_page.locations_with_filter_button)
        location_page.wait.until(lambda s: location_page.show_locations_list_with_checkbox[11].is_displayed())
        location_page.show_locations_list_with_checkbox[6].click()
        location_page.wait.until(lambda s: location_page.is_events_location_first_library_in_list_displayed)

        store_results_count_after_filters = location_page.search_results_returned_count.text
        first_result = "".join(i for i in store_results_count if i.isdigit())
        second_result = "".join(i for i in store_results_count_after_filters if i.isdigit())

        location_page.library_in_list_name[0].text.should_not.match(library_name)
        second_result.should.be.lower_than(first_result)
