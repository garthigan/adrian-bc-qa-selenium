import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from pages.core.home import HomePage
from pages.events.home_page_events import EventsHome
from pages.events.events_locations import EventLocationPage


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Find by address or ZIP code - valid")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56138", "TestRail")
class TestC56137:
    def test_c56138(self):
        self.base_url = "https://chipublib.demo.bibliocommons.com/events"
        HomePage(self.driver, self.base_url).open()

        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_link_displayed)

        home_page_events.hours_and_location_link.click()
        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_textbox_displayed)
        zip_code = "60605"
        home_page_events.hours_and_location_textbox.send_keys(zip_code)

        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_search_link_displayed)
        home_page_events.hours_and_location_search_link.click()

        location_page = EventLocationPage(self.driver)
        location_page.wait.until(lambda s: location_page.is_location_next_page_button_displayed)

        len(location_page.library_in_list_name).should.equal(
            len(location_page.location_search_results_has_get_direction_links))

        location_page.location_library_information[0].click()
        location_page.wait.until(lambda s: location_page.is_google_pin_expanded_library_name_displayed)
        location_page.is_events_location_google_pin_expanded_email_displayed.should.be.true
