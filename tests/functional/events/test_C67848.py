import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
from selenium.webdriver.support.ui import Select
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C67848 - Daily- Event with overlapping Daily events")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/67848", "TestRail")
class TestC67848:
    def test_c67848(self):

        self.base_url = "https://chipublib.demo.bibliocommons.com/events/"

        # log in
        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("cpltest3", "60643")
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # navigate to events page
        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)

        title_name = "test_C67848"
        events_admin.title_textbox.send_keys(title_name)
        events_admin.type_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_displayed)
        events_admin.audience_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        events_admin.description_field.send_keys("An event about a comic book")
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[36].is_displayed())
        events_admin.start_time[36].click()
        events_admin.wait.until(lambda s: events_admin.is_event_creation_page_this_event_happens_dropdown_displayed)
        events_admin.event_creation_page_this_event_happens_dropdown.click()

        # location set
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_archer_height_displayed)
        events_admin.location_dropdown_archer.click()

        # this event happens daily
        events_admin.wait.until(lambda s: events_admin.event_creation_page_this_event_happens_dropdown)
        my_select = Select(events_admin.event_creation_page_this_event_happens_dropdown)
        my_select.select_by_value("daily")

        events_admin.wait.until(lambda s: events_admin.is_event_start_date_button_displayed)
        events_admin.start_date_button.click()
        events_admin.wait.until(lambda s: events_admin.calendar_next_month_button[0].is_displayed())
        events_admin.calendar_next_month_button[0].click()
        events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[13].is_displayed())
        events_admin.calendar_start_end_date_select[13].click()
        events_admin.wait.until(lambda s: events_admin.is_end_date_button_displayed)
        events_admin.end_date_button.click()
        events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[46].is_displayed())
        events_admin.calendar_start_end_date_select[46].click()

        # add another pattern
        events_admin.wait.until(lambda s: events_admin.is_add_another_date_date_time_pattern_displayed)
        events_admin.add_another_date_date_time_pattern.click()

        # this event happens daily second pattern
        events_admin.wait.until(lambda s: events_admin.is_event_creation_page_this_event_happens_dropdown_pattern_2_displayed)
        my_select = Select(events_admin.this_event_happens_dropdown_pattern_2_find[1])
        my_select.select_by_value("daily")
        events_admin.wait.until(lambda s: events_admin.start_date_button_2nd_find[1].is_displayed())
        events_admin.start_date_button_2nd_find[1].click()

        events_admin.wait.until(lambda s: events_admin.calendar_next_month_button[2].is_displayed)
        events_admin.calendar_next_month_button[2].click()
        events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[75].is_displayed())
        events_admin.calendar_start_end_date_select[75].click()
        events_admin.wait.until(lambda s: events_admin.end_date_button_2nd_find[1].is_displayed())
        events_admin.end_date_button_2nd_find[1].click()
        events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[110].is_displayed())
        events_admin.calendar_start_end_date_select[110].click()
        events_admin.wait.until(lambda s: events_admin.time_selectors[1].is_displayed())
        events_admin.time_selectors[1].click()
        events_admin.wait.until(lambda s: events_admin.start_time[192].is_displayed())
        events_admin.start_time[192].click()

        # publish event
        events_admin.wait.until(lambda s: EC.element_to_be_clickable(events_admin.is_save_and_publish_displayed))
        events_admin.save_and_publish.click()
        events_admin.wait.until(lambda s: events_admin.is_left_chevron_button_displayed)

        # search page for event and verify it appears
        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()

        events_admin.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        events_admin.event_name_last_word.text.should.match(title_name)

        # search published tab
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)

        events_admin.retry_search_until_results_appear(title_name)

        # Custom assertion to check if any overlapping events are created twice
        date_and_time_length = len(events_admin.date_and_time_published_tab)/2
        date_and_time_length = round(date_and_time_length)
        date = 0
        date_2 = 2
        time = 1
        time_2 = 3
        for x in range(1, date_and_time_length):
            if(events_admin.date_and_time_published_tab[date].text != events_admin.date_and_time_published_tab[date_2].text and
                    events_admin.date_and_time_published_tab[time].text == events_admin.date_and_time_published_tab[time_2].text):
                date += 2
                date_2 += 2
                time += 2
                time_2 += 2
            else:
                raise AssertionError
                print("FAILED Duplicate events dates with the same time.")
                exit()

        # delete all events
        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        delete_button = events_admin.admin_delete_button
        ActionChains(self.driver).move_to_element(delete_button).perform()
        events_admin.admin_delete_button.click()
        events_admin.delete_this_and_subsequent.click()

        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)
        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.assert_is_no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
        # Creates two events instead of one for overlapped events 50% of the time. Reported bug
