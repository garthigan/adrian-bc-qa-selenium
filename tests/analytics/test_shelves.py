import pytest
import allure
import sure
import sys
sys.path.append('tests')

from utils.bc_data_layer import DataLayer
from pages.core.v2.search_results import SearchResultsPage
from pages.core.home import HomePage

@pytest.fixture(scope='class')
def add_and_remove_from_shelves(request, selenium_setup_and_teardown):
    driver = request.cls.driver
    request.cls.datalayer = DataLayer(driver)
    base_url = "https://chipublib.demo.bibliocommons.com"
    home_page = HomePage(driver, base_url).open()
    home_page.header.log_in('cpltest2 ', '60605')
    home_page.header.search_for("a")
    search_page = SearchResultsPage(driver)
    search_page.search_result_items[0].add_to_shelf_combo_button.click()
    search_page.wait.until(
        lambda s: search_page.search_result_items[0].is_on_shelf_button_displayed)
    search_page.search_result_items[0].on_shelf_button.click()
    search_page.search_result_items[0].on_shelf_dropdown_button.click()
    search_page.search_result_items[0].remove_from_shelves.click()
    search_page.wait.until(
        lambda s: search_page.search_result_items[0].is_add_to_shelf_success_message_displayed)

@pytest.mark.analytics
@pytest.mark.usefixtures('add_and_remove_from_shelves')
class TestAnalyticsShelves:
    @allure.title("Analytics: Shelves: shelving a bib pushes event to dataLayer")
    # @allure.testcase("", "TestRail")
    def testShelvingBib(self):
        actual = self.datalayer.get_payload_for_event('bc.addToShelf')
        expected = {'gtmCategory': 'v2-shelves', 'gtmAction': 'v2-add', 'gtmValue': 1}
        for key, value in expected.items():
            actual.should.have.key(key).being.equal(value)

    @allure.title("Analytics: Shelves: removing a shelved bib pushes event to dataLayer")
    # @allure.testcase("", "TestRail")
    def testRemovingBib(self):
        actual = self.datalayer.get_payload_for_event('bc.removeFromShelf')
        expected = {'gtmCategory': 'v2-shelves', 'gtmAction': 'v2-remove', 'gtmValue': 1}
        for key, value in expected.items():
            actual.should.have.key(key).being.equal(value)
