#Basic Architectural Structure of BiblioCommons Selenium web tests project

##Page Object Model

Page Object Model (POM) is a widely used design pattern to enhance test maintenance and reduce code duplication. A PageObject is an object oriented class that models the interface of a given page.

Tests then use the functions defined in the PageObjects to interact with a given page - as the page design itself changes and UI elements are modified, typically the tests themselves do not require any changes, just the PageObjects need to be modified.

##Component Object Model

In BiblioCommons QA, we take this pattern a step further, breaking pages down into smaller components and in some cases, components themselves are made up of even smaller components - down to the smallest snippet that makes functional sense.

Consider a page that has an arbitrary carousel on it. The page itself is modeled with an appropriate PageObject, which contains one (or more) Carousel(s) (a ComponentObject), which in turn, contains one (or more) CarouselItem(s).

Specific examples used to test BiblioCommons Core will be given within this document to better illustrate these usages.

We use the PyPOM Python library to structure both our PageObjects and ComponentObjects - detailed documentation can be found at:

https://pypom.readthedocs.io/en/latest/

###Basic PageObject setup:

A PageObject should be defined for any page that has a unique URL route to it - e.g the Core Search page. All PageObject classes should be named `PageNamePage` (e.g `SearchPage`).

The exception to the URL route uniqueness is our BasePage class, which, as the name implies, is used as the base class for shared functionality. For BC Core, our BasePage class includes shared elements such as the page header and footer, as well as common functions, such as login functionality, etc. The BasePage class is then passed in as the parent class to any required individual PageObject, e.g: `class SearchPage(BasePage):`.

The core elements of PageObject classes are element identifiers and element objects (used to access a given element in a test or function). Let's look at our SearchPage class (found in bc-qa-selenium/tests/pages/core/search.py)in more detail:

```
# We define our class, inheriting from the BasePage object:
class SearchPage(BasePage):

    # We then set up a private variable (leading underscore_ in Python) referencing the CSS selector
    # for the given element. This is the reference most likely to change as the Core app changes:
    _search_query_locator = (By.CSS_SELECTOR, "[testid='field_boolquery']")
    
    # Note that we are not limited to only using CSS selectors as our element locator. All Selenium 
    # 'By.' references are supported as documented at: 
    # https://selenium-python.readthedocs.io/locating-elements.html. However, some locator strategies 
    # (like XPath) are best avoided.

    # With our element reference sorted, we can create a function that returns said element:
    @property
    def search_query(self):
        return self.find_element(*self._search_query_locator)
```

This is how we would then use our element within a test or other function:

```
# First we instantiate our PageObject reference:
search_page = SearchPage(self.driver)
# And then we can use the desired function, in this case, search_query, to do something
# appropriate for the element - e.g, send some keystrokes: 
search_page.search_query.send_keys("test text")
```

###Basic ComponentObject setup:

A ComponentObject is a smaller functional portion of a full page, typically doesn't have a unique URL route to it and can also be used multiple times within a given page (or shared across multiple PageObjects). Let's take a look at bc-qa-selenium/tests/pages/core/components/search_result_item.py:

```
# ComponentObjects inherit from PyPOM Region, not a given Page or PyPOM Page:
class SearchResultItem(Region):

    _bib_title_locator = (By.CSS_SELECTOR, "[data-key='bib-title']")

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)
```

As you may have noticed, locators and element functions work exactly the same as they do for PageObjects, they simply refer to elements within the Component instead. Things change back in the PageObject class that requires this component though. Since a SearchResultsPage object will have multiple search results (up to 10 per page), there's a reference in setting up the component to return multiple elements, not just the first match. Look at bc-qa-selenium/tests/pages/core/v2/search_results.py:

```
    # The locator will match multiple elements on the page:
    _search_result_item_locator = (By.CSS_SELECTOR, "[data-test-id='searchResultItem']")
    
    # Note the use of find_elements instead of find_element, which returns a list of matches:
    @property
    def search_result_items(self):
        return [SearchResultItem(self, element) for element in self.find_elements(*self._search_result_item_locator)]
```

Then, in a given test or function, we would use this like so:

```
    search_results_page = SearchResultsPage(self.driver)
    search_results_page.search_result_items[0].bib_title.click()
```

We can access subsequent items in the list with a valid index, [0] to [9] for a full page of results (but we can also use the list length to figure out if we got x number of results, etc). Notice how ComponentObjects do not need to be explicitly instantiated like PageObjects are. They are always called on the relevant PageObject that requires them.