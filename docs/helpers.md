#BiblioCommons Selenium web tests project 'Utils' functions and usage

In our `bc-qa-selenium` projects, our `utils` folder contains "helper" classes that are (or can be) used by all tests, to set up mock test data, interface with third party APIs and other dependencies, provide common functionality that doesn't fit into the PageObject model classes themselves, etc.

This document will be updated over time to list the available "utils" and their common usages/scenarios.

##TestConnector.User (bc-qa-selenium/tests/utils/bc_test_connector.py)

This class leverages the BC Test Connector PIN signals API to create test users. Import it into a required test:

```
from utils.bc_test_connector import TestConnector
```

And then use it within the test itself:

```
# Create a standard adult user:
user = TestConnector.User().create(self.driver)
# Create a "child" user (< 13 years old):
user = TestConnector.User(adult = False).create(self.driver)
```

This helper is currently WIP and will soon support adding fines, holds, checkouts and all other _current_ PIN signals API functionality:

https://bibliocommons.atlassian.net/wiki/spaces/DQA/pages/165753543/Test+Connector+PIN+API

##BCAPIGatewayBibs (bc-qa-selenium/tests/utils/bc_api_gateway.py)

This class leverages the BC API Gateway to search for and return Bib test data. Import it into a required test:

```
from utils.bc_api_gateway import BCAPIGatewayBibs
```

And then use it within the test itself:

```
bibs = BCAPIGatewayBibs(configuration.system.base_url).search(term = 'a')
```

This returns a list of Bibs, with each one having the following available properties:

```
bibs[0].id
bibs[0].format
bibs[0].type
bibs[0].provider
bibs[0].title
bibs[0].subtitle
bibs[0].availability.total
bibs[0].availability.available
bibs[0].availability.on_hold
```

The `search()` function of the BCAPIGatewayBibs class can take the following arguments:

```
term
search_type
format
provider
available
holds
```

Note that most of these are set to the following default values:

```
term
search_type = "keyword"
format = "BK"
provider = "ILS"
available = 1
holds = 0
```

This allows us to search for Bibs that meet numerous criteria, depending on the needs of our test(s). Some examples:

```
bibs = BCAPIGatewayBibs(Library).search(term = "a")
```

This will perform a keyword based search, for the term "a", returning physical books (BK format) provided by the ILS, with 0 holds and 1 available copy. Since we didn't specify anything other than the search term, default values were used in the search.

We can perform searches with additional arguments:

```
bibs = BCAPIGatewayBibs(Library).search(term = "Tom Clancy", search_type = "Author")
```

In this example, we'd still get back physical books from the ILS, with 0 holds and 1 available copy, since we didn't specify any of those properties. However, the search results would search for the term "Tom Clancy", as the author (instead of the default keyword search).

```
bibs = BCAPIGatewayBibs(Library).search(term = "a", format = 'EBOOK', provider = "OverDriveAPI")
```

This search is identical to our first example above, but would return eBooks, provided by OverDrive.

```
bibs = BCAPIGatewayBibs(Library).search(term = "a", format = 'EBOOK', provider = "MMM")
```

Similar search, but returns eBooks provided by 3M instead.